var secureSend = require("../lib/secure-send.js")
  , should = require("should")
  , Q = require("Q");

var crypto;

describe('crypto', function() {
  before(function() {
    crypto = new secureSend.crypto();
  });
  
  it('should have defaults we expect', function() {
    crypto.defaultEncoding.should.equal('utf8');
    crypto.encryptionEncoding.should.equal('base64');
    crypto.encryptionMethod.should.equal('aes-256-cbc');
  });
  
  describe('#encrypt', function() {
    it('requires a password', function() {
      var plaintext = 'abc123';
      
      Q(plaintext)
      .then(crypto.encrypt(null))
      .should.throw();
    });
    
    it('should return garbled text', function() {
      var crypto = new secureSend.crypto();
      var plaintext = 'abc123';
      var password = 'password1';
      
      Q(plaintext)
        .then(crypto.encrypt(password))
        .done(function (data) {
          data.should.not.equal(plaintext);
        });
    });
  });
  
  describe('#decrypt', function() {
    it('requires a password', function() {
      var plaintext = 'abc123';
      var password = 'password1';
      
      Q(plaintext)
        .then(crypto.encrypt(password))
        .then(crypto.decrypt(null))
        .should.throw();
    });

    it('should be able to decrypt encrypted text', function() {
      var crypto = new secureSend.crypto();
      var plaintext = 'abc123';
      var password = 'password1';
      
      Q(plaintext)
        .then(crypto.encrypt(password))
        .then(crypto.decrypt(password))
        .done(function (data) {
          data.should.equal(plaintext);
        });
    });
    
    it('should not decrypt if the password is incorrect', function() {
      var crypto = new secureSend.crypto();
      var plaintext = 'abc123';
      var encryptPassword = 'password1';
      var decryptPassword = 'password2';
      
      Q(plaintext)
        .then(crypto.encrypt(encryptPassword))
        .then(crypto.decrypt(decryptPassword))
        .should.throw();
    });
  });
});