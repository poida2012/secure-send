var secureSend = require("../lib/secure-send.js")
  , should = require("should")
  , sinon = require("sinon");

var user = 'root'
  , host = 'hostname.com'
  , port = 22
  , log = console.log
  , connectionFactory = {
      create : function() {
        return null;
      }
    }
  , deferFactory = {
      create : function() {
        return null;
      }
    };

var client;

describe('client', function() {
  it('should have defaults we expect',
      function() {
        client = new secureSend.client(user, host, port, log, connectionFactory,
            deferFactory);
        client.agent.should.equal('pageant');
        client.defaultEncoding.should.equal('utf8');
      });

  describe('#ctor', function() {
    it('should set the object properties', function() {
      client = new secureSend.client(user, host, port, log, connectionFactory,
          deferFactory);
      client.user.should.equal(user);
      client.host.should.equal(host);
      client.port.should.equal(port);
      client.log.should.equal(log);
      client.connectionFactory.should.equal(connectionFactory);
      client.deferFactory.should.equal(deferFactory);
    });
  });
  
  describe('#connect', function() {
    it('does not require a password', function() {

      var sftp = {};
      var createSSH = function() {
        return {
          connect: function(args) {
            if (ssh.ready) {
              ssh.ready(null, sftp);
            }
          },
          on: function(event, cb) {
            ssh.event = cb;
          }
        }
      };
      
      
      client = new secureSend.client(user, host, port, log, { create: createSSH }, null);
      client.connect(null);
      
    });
    
    it('calls the ssh connect method', function() {
      throw 'TODO';
    });
  });
  
  describe('#copyFile', function() {
    it('requires both parameters', function() {
      throw 'TODO';
    });
    it('only works when the client is connected', function() {
      throw 'TODO';
    });
    it('calls the sftp fastPut method', function() {
      throw 'TODO';
    });
  });
  
  describe('#disconnect', function() {
    it('only works when the client is connected', function() {
      throw 'TODO';
    });
    it('calls the ssh disconnect method', function() {
      throw 'TODO';
    });
  });
  
  describe('#exec', function() {
    it('requires both parameters', function() {
      throw 'TODO';
    });
    it('only works when the client is connected', function() {
      throw 'TODO';
    });
    it('calls the sftp fastPut method', function() {
      throw 'TODO';
    });
  });
  
  describe('#mkdir', function() {
    it('requires the remoteDirectory parameter', function() {
      throw 'TODO';
    });
    it('only works when the client is connected', function() {
      throw 'TODO';
    });
    it('calls the sftp mkdir method', function() {
      throw 'TODO';
    });
  });
  
  describe('#writeBuffer', function() {
    it('requires the remotePath parameter', function() {
      throw 'TODO';
    });
    it('only works when the client is connected', function() {
      throw 'TODO';
    });
    it('calls the sftp write method', function() {
      throw 'TODO';
    });
  });

});