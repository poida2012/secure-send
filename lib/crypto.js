var Crypto = require('crypto')
  , fs = require('fs')
  , Q = require('q');

/**
 * Encryption class built with Q promises and the node crypto library.
 * @class crypto
 * @constructor
 */
function crypto() {
	
}

/**
 * The encryption method used to create the cipher and decipher objects.
 * 
 * @property encryptionMethod
 * @type {String}
 * @default "aes-256-cbc"
 */
crypto.prototype.encryptionMethod = 'aes-256-cbc';

/**
 * The encoding used to treat the encrypted data.
 * 
 * @property encryptionEncoding
 * @type {String}
 * @default "base64"
 */
crypto.prototype.encryptionEncoding = 'base64';

/**
 * The encoding used to treat the plaintext data.
 * 
 * @property defaultEncoding
 * @type {String}
 * @default "utf8"
 */
crypto.prototype.defaultEncoding = 'utf8';

/**
 * Encrypt data.
 * 
 * @example
 *      Q.nfcall(fs.readFile, 'plaintext.txt')
 *       .then(fsCrypto.encrypt('password1'))
 *       .then(console.log)
 *       .done();
 * 
 * @method encrypt
 * 
 * @param {String} password The password used to encrypt the data.
 * 
 * @return {Function} A function() to create promise to encrypt data with the given
 * password. The promise as input expects the unencrypted data. The promise will
 * be resolved with the encrypted data.
 */
crypto.prototype.encrypt = function(password) {
	var self = this;
	return Q.fbind(function(plaintext) {
		var cipher = Crypto.createCipher(self.encryptionMethod, password);
	
		var secret = cipher.update(plaintext, self.defaultEncoding, self.encryptionEncoding);
		return secret + cipher.final(self.encryptionEncoding);
	});
};

/**
 * Decrypt encrypted data.
 * 
 *      Q.nfcall(fs.readFile, 'encrypted.txt')
 *       .then(fsCrypto.decrypt('password1'))
 *       .then(console.log)
 *       .done();
 * 
 * @method decrypt
 * 
 * @param {String} password The password used to encrypt the data initially.
 * 
 * @return {Function} A function() to create promise to decrypt data with the given
 * password. The promise as input expects the encrypted data. The promise will be
 * resolved with the decrypted data.
 */
crypto.prototype.decrypt = function(password) {
	var self = this;
	return Q.fbind(function(secret) {
		var decipher = Crypto.createDecipher(self.encryptionMethod, password);

		var plaintext = decipher.update(secret.toString(self.defaultEncoding), self.encryptionEncoding, self.defaultEncoding);
		return plaintext + decipher.final(self.defaultEncoding);
	});	
};

module.exports = crypto;