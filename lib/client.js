var Connection = require('ssh2')
    , Q = require('q');

/**
 * Provides an SSH client with methods build on Q promises and the SSH2 library.
 * An instance of the class represents a single connection for a user to a given
 * host.
 * 
 * @example
 *     var client = new client('root', '127.0.0.1', 22, console.log);
 *     client.connect()
 *       .then(client.mkdir('/opt/newdir'))
 *       .then(client.copyFile('newdir-1.tar.gz', '/opt/newdir'))
 *       .then(client.copyFile('newdir-2.tar.gz', '/opt/newdir'))
 *       .then(client.disconnect)
 *       .done();
 *   
 * @class client
 * @constructor
 * 
 * @param {String} user SSH username.
 * @param {String} host Remote host to connect to.
 * @param {Integer} [port] SSH port to connect on. If null defaults to 22.
 * @param {Function} [log] Logging function with signature function (logMessage).
 * 
 * @param {Object} [connectionFactory] SSH connection factory.
 *     @param {Function} connectionFactory.create A function () to create a new ssh connection.
 *    
 * @param {Object} [deferredFactory] Defer factory. If null will default to Q.defer.
 *     @param {Function} deferredFactory.create A function () to create a new defer object.
 *   
 */
function client(user, host, port, log, connectionFactory, deferredFactory) {
	this.user = user;
	this.host = host;
	this.port = port || 22;
	this.log = log || function(msg) {};
	this.connectionFactory = connectionFactory || { create: function() { return new Connection(); } };
	this.deferFactory = deferredFactory || { create: Q.defer };
}

/**
 * The authentication agent to be used by the SSH client if no password is specified.
 * 
 * @property agent
 * @type {String}
 * @default "pageant"
 */
client.prototype.agent = 'pageant';

/**
 * The default string encoding used by the SSH client.
 * 
 * @property defaultEncoding
 * @type {String}
 * @default "utf8"
 */
client.prototype.defaultEncoding = 'utf8';

/**
 * Connect via SSH to the host specified by the host property. On success, this
 * method sets the ssh and sftp properties.
 * 
 * @example
 * 
 *      var client = new client('root', '127.0.0.1', 22);
 *      client.connect()
 *       .then(client.disconnect)
 *       .done();
 * 
 * @method connect
 * 
 * @param {String} [password] SSH password. If null agent authentication will be
 * used.
 * 
 * @return {Promise} A promise to connect to the remote host.
 */
client.prototype.connect = function(password) {
	var self = this
	  , ssh = self.connectionFactory.create()
	  , deferred = self.deferFactory.create();
	
	self.log('SSH: Connecting to ' + self.host + ' on port ' + self.port + ' as ' + self.user + '.');
	
	ssh.on('ready', function() {
		ssh.sftp(function(err, sftp) {
			if (err) {
				deferred.reject(err);
			} else {
				self.log('SSH: Connection to ' + self.host + ' on port ' + self.port + ' as ' + self.user + ' established.');
				
				self.ssh = ssh;
				self.sftp = sftp;
				
				deferred.resolve();
			}
		});
	});
	
	ssh.on('error', function(err) {
		deferred.reject(err);
	});
	
	if (password) {
		ssh.connect({host: self.host, port: self.port, username: self.user, password: password });
	} else {
		ssh.connect({host: self.host, port: self.port, username: self.user, agent: self.agent });
	}
	
	return deferred.promise;
};

/**
 * Close the SSH connection. On success the ssh and sftp properties are set to
 * null.
 * 
 * @example
 * 
 *     client.connect()
 *       .then(client.disconnect)
 *       .done();
 * 
 * @method disconnect
 * @return {Promise} A promise to disconnect from the remote host. No data is passed
 * to the next method in the chain.
 */
client.prototype.disconnect = function () {
	var self = this
	  , deferred = self.deferFactory.create();

	self.log('SSH: Closing connection to ' + self.host + '.');
	
	self.ssh.on('close', function() {
		self.log('SSH: Connection to ' + self.host + ' closed.');
		self.ssh = null;
		self.sftp = null;
		deferred.resolve();
	});
	
	self.ssh.on('error', function() {
		deferred.reject();
	});
	
	self.ssh.end();

	return deferred.promise;
};

/**
 * Create a remote directory. The command executed is equivalent
 * to "mkdir -p $remoteDiretory" from the shell.
 * 
 * @example
 * 
 *     client.connect()
 *       .then(client.mkdir('/opt/new/directory'))
 *       .then(client.disconnect)
 *       .done();
 *       
 * @method mkdir
 * 
 * @param remoteDirectory The remote directory to create.
 * 
 * @return {Function} A function that creates a promise to create a directory on
 * the remote host.
 */
client.prototype.mkdir = function (remoteDirectory) {
	var self = this;
	return function() {
		var deferred = self.deferFactory.create();
		self.log('SSH: Creating directory ' + remoteDirectory + '.');
		
		self.sftp.mkdir(remoteDirectory, function(err) {
			if (err) {
				deferred.reject(err);
			} else {
				self.log('SSH: Directory ' + remoteDirectory + ' created.');
				deferred.resolve();
			}
		});
	
		return deferred.promise;
	};
};

/**
 * Write a buffer to a remote file. The data should be passed to the promise through
 * the fulfilment value of the previous promise in the chain.
 * 
 * @example
 * 
 *     client.connect()
 *       .then(Q.nfcall(fs.readFile, 'tmp.txt'))
 *       .then(client.writeBuffer('/tmp/remotefile.txt'))
 *       .then(client.disconnect)
 *       .done();
 *       
 * @method writeBuffer
 * 
 * @param remotePath The fully qualified remote path to create.
 * 
 * @return {Function} A function that creates a promise to write the given data
 * to disk on the remote host.
 */
client.prototype.writeBuffer = function (remotePath) {
	var self = this;
	return function(data) {
		var deferred = self.deferFactory.create();
		self.log('SSH: Writing buffer to ' + remotePath + '.');
		
		var ws = self.sftp.createWriteStream(remotePath);
		ws.write(data, this.defaultEncoding, function(err) {
			if (err)  {
				deferred.reject(err);
			} else {
				self.log('SSH: Buffer written to ' + remotePath + '.');
				deferred.resolve();
			}
		});
		
		return deferred.promise;
	};
};

/**
 * Copy a local file to the remote host.
 * 
 * @example
 * 
 *     client.connect()
 *       .then(client.copyFile('localfile.txt', '/tmp/remotefile.txt'))
 *       .then(client.disconnect)
 *       .done();
 * 
 * @method copyFile
 * 
 * @param localPath The fully qualified local path to copy.
 * 
 * @param remotePath The fully qualified path of the file to create on the remote
 * host.
 * 
 * @return {Function} A function () to create a promise to copy the local file
 * to the remote host with the remotePath.
 */
client.prototype.copyFile = function (localPath, remotePath) {
	var self = this;
	return function() {
		var deferred = self.deferFactory.create();
		self.log('SSH: Copying local file ' + localPath + ' to ' + remotePath + '.');
		
		self.sftp.fastPut(localPath, remotePath, function(err) {
			if (err) {
				deferred.reject(err);
			} else {
				self.log('SSH: Local file ' + localPath + ' copied to ' + remotePath + '.');
				deferred.resolve();
			}
		});
		
		return deferred.promise;
	};
};

/**
 * The command to execute on the remote host.
 * 
 * @example
 * 
 *     client.connect()
 *       .then(client.exec('echo hello world from remote host'))
 *       .then(client.disconnect())
 *       .done();
 * 
 * @method exec
 * 
 * @param command The command to execute on the remote host.
 * 
 * @return {Function} A function () that creates a promise to execute the command
 * on the remote host.
 * 
 * The promise will be resolved with the commands channel stream.
 */
client.prototype.exec = function (command) {
	var self = this;
	return function() {
		var deferred = self.deferFactory.create();
		self.log('SSH: Executing command \'' + command + '\'.');
		
		self.ssh.exec(command, function(err, stream) {
			if (err) {
				deferred.reject(err);
			} else {
				self.log('SSH: Command \'' + command + '\' executed.');
				deferred.resolve(stream);
			}
		});
	
		return deferred.promise;
	};
};

module.exports = client;
