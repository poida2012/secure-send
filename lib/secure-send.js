var client = require("./client.js")
  , crypto = require("./crypto.js");

/**
 * The secure-send module provides simple classes for encryption, decryption and
 * ssh/sftp.
 * 
 * @module secure-send
 * @main secure-send
 * @class secure-send
 * @static
 */
module.exports = {
  /**
   * The ssh client class.
   * 
   * @example
   *     var secureSend = require("secure-send");
   *     var client = new secureSend.client("username", "host", 22);
   * 
   * @property client
   * @type client
   */
  "client": client,
  
  /**
   * The crypto class.
   * 
   * @example
   *     var secureSend = require("secure-send");
   *     var crypto = new secureSend.crypto();
   * 
   * @property crypto
   * @type crypto
   */
  "crypto": crypto
};